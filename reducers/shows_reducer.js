import {
  FETCH_SHOWS,
  FETCH_SINGLE_SHOW,
  FETCH_DISCOVER,
  USER_SHOWS_FETCH,
  USER_SHOWS_FETCH_SUCCESS,
  SHOW_UPDATE_SUCCESS,
  TEXT_CHANGED,
  FETCH_DISCOVER_SUCCESS
} from '../actions/types';

const INITIAL_STATE = {
  singleShow: {},
  shows: [],
  discoveredShows: [],
  userShows: [],
  text: '',
  loading: false,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_SHOWS:
      return { ...state, shows: action.payload };
    case FETCH_SINGLE_SHOW:
      return { ...state, singleShow: action.payload };
    case FETCH_DISCOVER:
      return { ...state, loading: true };
    case FETCH_DISCOVER_SUCCESS: {
      return { ...state, discoveredShows: action.payload, loading: false };
    }
    case USER_SHOWS_FETCH:
      return { ...state, loading: true };
    case USER_SHOWS_FETCH_SUCCESS:
      return { ...state, userShows: action.payload, loading: false };
    case SHOW_UPDATE_SUCCESS:
      return { ...state };
    case TEXT_CHANGED:
      return { ...state, text: action.payload };
    default:
      return state;
  }
};
