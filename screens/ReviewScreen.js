import React, { Component } from 'react';
import {
  ScrollView,
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
  Modal
} from 'react-native';
import { Card, Badge, SearchBar, Rating, Button, Icon } from 'react-native-elements';
import ActionButton from 'react-native-action-button';
import { connect } from 'react-redux';
import _ from 'lodash';

import * as actions from '../actions';
import { Spinner } from '../components/Spinner';

class ReviewScreen extends Component {
  static navigationOptions = {
    title: 'My Shows',
    tabBarIcon: ({ tintColor }) => {
      return <Icon name="home" size={30} color={tintColor} />;
    }
  }

  state = { searching: false, modalVisible: false, selectedShow: '' };

  componentWillMount() {
      this.props.showsFetch();
  }

  onButtonPress(id, lastSeen) {
   this.props.fetchSeasonsForShow(id, lastSeen, () => {
     this.props.navigation.navigate('showDetail');
   });
 }

  renderImage(image) {
    if (_.isNull(image)) {
      return <View />;
    }
    return (
      <View style={styles.imageContainerStyle}>
        <Image source={{ uri: image.original }} style={styles.imageStyle} />
      </View>
    );
  }

  onTextChange(text) {
    this.props.textChanged(text);
  }

  ratingCompleted(rating) {
    this.props.saveShowRating(this.state.selectedShow, rating);
  }

  renderBadge(rating) {
    if (!_.isNil(rating)) {
      return (
        <View style={{ alignItems: 'flex-end' }}>
          <Badge value={rating} containerStyle={{ backgroundColor: '#03A9F4' }} />
        </View>
      );
    }
  }

  renderShows() {
    const sortedList = _.sortBy(this.props.shows, ['rating']).reverse();
    return _.map(sortedList, show => {
      const { id, image, name } = show.show.show;
      const remaining = _.isNil(show.lastSeen) ?
        0 : show.show.number - (show.lastSeen.season * show.lastSeen.number);
      return (
          <Card key={id}>
            {this.renderBadge(show.rating)}
            <TouchableWithoutFeedback
              onPress={() => this.onButtonPress(id, show.lastSeen)}
              onLongPress={() => this.setState({
                 modalVisible: true, selectedShow: id })}
            >
            <View style={styles.wrapperStyle}>
              {this.renderImage(image)}
              <View style={styles.detailWrapper}>
                <Text style={{ fontSize: 14, fontWeight: 'bold' }}>{name}</Text>
                <Text style={styles.italics}>
                  {_.isNil(show.lastSeen) ?
                    'Not yet selected' :
                    `${show.lastSeen.season}${'X'}${show.lastSeen.number}${' '}${show.lastSeen.name.substring(0, 100)}` }
                </Text>
                <Text style={styles.italics}>
                  {_.isNil(show.lastSeen) ? '' : show.lastSeen.airdate}
                </Text>
                <Text style={styles.italics}>
                  {_.isNil(show.lastSeen) ? '' : `${'Remaining: '}${remaining}`}
                </Text>
              </View>
            </View>
            </TouchableWithoutFeedback>
            <View style={{ paddingTop: 10 }}>
            <Icon
              name='trash'
              type='font-awesome'
              size={20}
              onPress={() => this.props.deleteUserShow(id)}
            />
            </View>
          </Card>
      );
    });
  }

  renderSearchBar() {
    if (this.state.searching) {
      return (
        <View>
        <SearchBar
          round
          lightTheme
          onChangeText={text => this.onTextChange(text)}
          onSubmitEditing={() => this.props.fetchShowsByName(this.props.text, () => {
            this.props.navigation.navigate('showsList');
          })}
          placeholder='Type Here...'
          containerStyle={{ marginTop: 20 }}
        />
        </View>
      );
    }
  }

  renderScreen() {
    if (this.props.loading) {
      return (
        <View style={{ flex: 1 }}>
          <Spinner size="large" />
        </View>
      );
    }
    return (
      <View style={{ flex: 1 }}>
        <View>
          {this.renderSearchBar()}
        </View>
        <ScrollView style={{ marginTop: 10 }}>
          {this.renderShows()}
        </ScrollView>
        <ActionButton
          buttonColor="#03A9F4"
          onPress={() => this.setState({ searching: !this.state.searching })}
        />
        <Modal
          animationType={'slide'}
          transparent
          visible={this.state.modalVisible}
          onRequestClose={() => {}}
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center'
          }}
        >
         <View
          style={{
           backgroundColor: 'rgba(0,0,0,0.75)',
           position: 'relative',
           flex: 1,
           justifyContent: 'center'
         }}
         >
         <Rating
           showRating
           type="star"
           fractions={1}
           startingValue={0}
           readonly
           ratingCount={10}
           imageSize={37}
           onFinishRating={value => this.ratingCompleted(value)}
           style={{ backgroundColor: '#fff', marginBottom: 10, paddingVertical: 100 }}
         />
         <Button
          onPress={() => this.setState({ modalVisible: false })}
          title="I am done"
          icon={{ name: 'done' }}
          backgroundColor='#03A9F4'
          borderRadius={75}
         />
        </View>
      </Modal>
      </View>
    );
  }

  render() {
    return (
        this.renderScreen()
    );
  }
}

const styles = {
  wrapperStyle: {
    justifyContent: 'flex-start',
    flexDirection: 'row',
    position: 'relative'
  },
  detailWrapper: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    marginLeft: 10
  },
  italics: {
    fontStyle: 'italic'
  },
  imageContainerStyle: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  imageStyle: {
    height: 100,
    width: 100
  },
  iconStyle: {
    position: 'absolute',
    bottom: 10,
    right: 10,
  }
};

const mapStateToProps = ({ addedShows }) => {
  return {
    shows: addedShows.userShows,
    text: addedShows.text,
    loading: addedShows.loading
  };
};

export default connect(mapStateToProps, actions)(ReviewScreen);
