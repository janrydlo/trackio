import React, { Component } from 'react';
import { View, Text, ScrollView, TouchableOpacity, Image, Linking } from 'react-native';
import { Card, Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import _ from 'lodash';
import * as Progress from 'react-native-progress';
import * as actions from '../actions';

class ShowDetailScreen extends Component {

  static navigationOptions = {
    title: 'Show Detail'
  }

  onButtonPress(season) {
   this.props.fetchEpisodes(this.props.show.id, season, this.props.lastSeen, () => {
     this.props.navigation.navigate('episodesReview');
   });
 }

 getProgress(seasonObj) {
   if (_.isNil(this.props.lastSeen)) {
     return 0;
   }

   const { season, number } = this.props.lastSeen;
   if (season > seasonObj.number) {
     return 1;
   }
   if (season < seasonObj.number) {
     return 0;
   }
   return (number / seasonObj.episodeOrder);
 }

 renderImage(image) {
   if (_.isNull(image)) {
     return <View />;
   }
   return (
     <View>
       <Image source={{ uri: image.original }} style={{ height: 150 }} />
     </View>
   );
 }

 getWatchEpisodesString(season) {
   const episodeCount = _.isNil(season.episodeOrder) ? ' Not available' : season.episodeOrder;

   if (_.isNil(this.props.lastSeen)) {
     return `${'0'}${' / '}${episodeCount}`;
   }
   if (this.props.lastSeen.season === season.number) {
     return `${this.props.lastSeen.number}${' / '}${episodeCount}`;
   }
   if (this.props.lastSeen.season > season.number) {
     return `${season.episodeOrder}${' / '}${episodeCount}`;
   }
   if (this.props.lastSeen.season < season.number) {
     return `${'0'}${' / '}${episodeCount}`;
   }
 }

  renderSeasons() {
    return _.map(this.props.seasons, season => {
      return (
        <TouchableOpacity
          onPress={() => this.onButtonPress(season.number)}
           key={season.id}
        >
          <Card title={`${'Season '}${season.number}`} >
            <View
              style={{ flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center' }}
            >
            <Text>{this.getWatchEpisodesString(season)}</Text>
            </View>
            <Progress.Bar progress={this.getProgress(season)} width={310} />
          </Card>
        </TouchableOpacity>
      );
    });
  }

  renderShow() {
    const IMDB_URL = 'http://www.imdb.com/title/';
    const TVDB_URL = 'http://thetvdb.com/?tab=series&id='

    const { name, summary, image, rating, externals } = this.props.show;
    return (
      <Card title={name}>
        {this.renderImage(image)}
        <Text
        style={styles.ratingStyle}
        >
        {`${_.isNil(rating.average) ? 'N/A' : rating.average}${' / 10'}`}
        </Text>
        <Text>{summary.replace(/<[^>]*>/g, '')}</Text>
        <View
        style={{ flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10 }}
        >
            <View style={{ flexGrow: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Icon
            reverse
            name='ios-link'
            type='ionicon'
            color='#03A9F4'
            size={15}
            onPress={() => Linking.openURL(`${IMDB_URL}${externals.imdb}`)}
          />
          <Text>IMDB</Text>
          </View>
          <View style={{ flexGrow: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Icon
            reverse
            name='ios-link'
            type='ionicon'
            color='#03A9F4'
            size={15}
            onPress={() => Linking.openURL(`${TVDB_URL}${externals.thetvdb}`)}
          />
          <Text>TVDB</Text>
          </View>
        </View>
      </Card>
    );
  }

  render() {
    return (
      <ScrollView>
        {this.renderShow()}
        <View>
          {this.renderSeasons()}
        </View>
      </ScrollView>
    );
  }
}

const styles = {
  ratingStyle: {
    textAlign: 'center',
    fontSize: 25,
    fontWeight: 'bold',
    marginTop: 10,
    marginBottom: 10
  }
};

const mapStateToProps = ({ seasons }) => {
  return {
    seasons: seasons.fetchedSeasons,
    show: seasons.show,
    lastSeen: seasons.lastSeen
  };
};

export default connect(mapStateToProps, actions)(ShowDetailScreen);
