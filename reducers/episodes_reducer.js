import {
  FETCH_EPISODES
} from '../actions/types';

const INITIAL_STATE = {
  results: [],
  season: '',
  idShow: '',
  lastSeen: {}
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_EPISODES:
      return {
        results: action.payload.episodes,
        season: action.payload.season,
        idShow: action.payload.idShow,
        lastSeen: action.payload.lastSeen
      };
    default:
      return state;
  }
};
