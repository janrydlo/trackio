import { combineReducers } from 'redux';
import auth from './auth_reducer';
import addedShows from './shows_reducer';
import episodes from './episodes_reducer';
import seasons from './seasons_reducer';

export default combineReducers({
  auth,
  addedShows,
  episodes,
  seasons
});
