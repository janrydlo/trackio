import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import {
  Button,
  Card,
  FormInput,
  FormLabel,
  FormValidationMessage,
} from 'react-native-elements';

import * as actions from '../actions';
import { Spinner } from '../components/Spinner';

class RegistrationScreen extends Component {

  static navigationOptions = {
    title: 'Registration Screen'
  }

  onEmailChange(text) {
    this.props.emailChanged(text);
  }

  onPasswordChange(text) {
    this.props.passwordChanged(text);
  }

  onRepeatedPasswordChange(text) {
    this.props.repeatedPasswordChanged(text);
  }

  onButtonPress() {
    const { email, password, repeatedPassword } = this.props;
    this.props.registerUser({ email, password, repeatedPassword }, () => {
      this.props.navigation.navigate('auth');
     });
  }

  renderButton() {
    if (this.props.loading) {
      return (
        <Spinner size="large" />
      );
    }
    return (
      <Button
      onPress={this.onButtonPress.bind(this)}
      title="Register"
      icon={{ name: 'done' }}
      backgroundColor='#03A9F4'
      borderRadius={75}
      />
    );
  }

  render() {
    return (
      <View>
      <Card>
        <View>
          <FormLabel>Email</FormLabel>
          <FormInput
            placeholder="email@gmail.com"
            onChangeText={this.onEmailChange.bind(this)}
            value={this.props.email}
          />
        </View>
        <View>
          <FormLabel>Password</FormLabel>
          <FormInput
            placeholder="password"
            secureTextEntry
            onChangeText={this.onPasswordChange.bind(this)}
            value={this.props.password}
          />
        </View>
        <View>
          <FormLabel>Repeat password</FormLabel>
          <FormInput
            placeholder="Repeat Password"
            secureTextEntry
            onChangeText={this.onRepeatedPasswordChange.bind(this)}
            value={this.props.repeatedPassword}
          />
        </View>

        <FormValidationMessage>{this.props.error}</FormValidationMessage>

        <View>
          {this.renderButton()}
        </View>
      </Card>
      </View>
    );
  }
}

const mapStateToprops = ({ auth }) => {
  const { email, password, repeatedPassword, error, loading } = auth;

  return { email, password, repeatedPassword, error, loading };
};

const styles = {
  errorTextStyle: {
    fontSize: 20,
    alignSelf: 'center',
    color: 'red'
  }
};

export default connect(mapStateToprops, actions)(RegistrationScreen);
