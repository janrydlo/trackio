import axios from 'axios';

import {
  FETCH_SHOW_SEASONS
} from './types';

const BASE_API_URL = 'http://api.tvmaze.com';

export const fetchSeasonsForShow = (idShow, lastSeen, callback) => async (dispatch) => {
  try {
    let data = await axios.get(`${BASE_API_URL}${'/shows/'}${idShow}${'/seasons'}`);
    let show = await axios.get(`${BASE_API_URL}${'/shows/'}${idShow}`);

    dispatch({
      type: FETCH_SHOW_SEASONS,
      payload: { seasons: data.data, show: show.data, lastSeen } });
    callback();
  } catch (e) {
    console.log(e.message);
  }
};
