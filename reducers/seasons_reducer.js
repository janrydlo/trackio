import {
  FETCH_SHOW_SEASONS
} from '../actions/types';

const INITIAL_STATE = {
  fetchedSeasons: [],
  show: {},
  lastSeen: {}
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_SHOW_SEASONS:
      return {
        ...state,
        fetchedSeasons: action.payload.seasons,
        show: action.payload.show,
        lastSeen: action.payload.lastSeen
      };
    default:
      return state;
  }
};
