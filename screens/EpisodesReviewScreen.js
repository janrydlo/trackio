import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';
import { Card, Button, Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import _ from 'lodash';

import * as actions from '../actions';

class EpisodesReviewScreen extends Component {
  static navigationOptions = {
    title: 'Season Episodes'
  }

  onButtonPress(episode) {
    this.props.saveLastEpisode(this.props.idShow, episode, () => {
      this.props.navigation.navigate('review');
    });
  }

  renderIcon(episodeNumber) {
    if (_.isNil(this.props.lastSeen)) {
      return;
    }

    const { season, number } = this.props.lastSeen;
    if (season > this.props.season) {
      return (
        <Icon
          reverse
          name='check'
          size={15}
          type='font-awesome'
          color='#00E676'

        />
      );
    }
    if (season === this.props.season && episodeNumber <= number) {
      return (
      <Icon
        reverse
        name='check'
        size={15}
        type='font-awesome'
        color='#00E676'

      />
      );
    }
  }

  renderEpisodes() {
      const seasonEpisodes = this.props.episodes.filter((episode) => {
        return episode.season === this.props.season;
      });

      return seasonEpisodes.map(episode => {
        const { id, name, number, airdate, summary } = episode;
        return (
          <Card key={id} title={`${name}${' ('}${number}${')'}`}>
            <View
            style={{ flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-around',
            marginBottom: 10
           }}
            >
            <Text>{`${'AirDate: '}${airdate}`}</Text>
            {this.renderIcon(number)}
            </View>
            <View style={{ marginBottom: 10 }}>
              <Text>{_.isNil(summary) ? ' ' : summary.replace(/<[^>]*>/g, '')}</Text>
            </View>
            <View style={{ paddingTop: 10 }}>
              <Button
                title="Set as last seen"
                icon={{ name: 'done' }}
                backgroundColor='#03A9F4'
                borderRadius={75}
                onPress={() => this.onButtonPress(episode)}
              />
            </View>
          </Card>
        );
      });
  }

  render() {
    return (
      <ScrollView>
        {this.renderEpisodes()}
      </ScrollView>
    );
  }
}

const mapStateToProps = ({ episodes }) => {
  return {
    episodes: episodes.results,
    season: episodes.season,
    idShow: episodes.idShow,
    lastSeen: episodes.lastSeen
  };
};

export default connect(mapStateToProps, actions)(EpisodesReviewScreen);
