import axios from 'axios';

import {
  FETCH_EPISODES
} from './types';

const BASE_API_URL = 'http://api.tvmaze.com';

export const fetchEpisodes = (idShow, season, lastSeen, callback) => async (dispatch) => {
  try {
    let { data } = await axios.get(`${BASE_API_URL}${'/shows/'}${idShow}${'/episodes'}`);
    dispatch({ type: FETCH_EPISODES, payload: { episodes: data, season, idShow, lastSeen } });
    callback();
  } catch (e) {
    console.log(e.message);
  }
};
