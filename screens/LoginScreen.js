import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import {
  Button,
  Card,
  FormInput,
  FormLabel,
  FormValidationMessage
} from 'react-native-elements';

import * as actions from '../actions';
import { Spinner } from '../components/Spinner';
import { Header } from '../components/Header';

class LoginForm extends Component {

  static navigationOptions = {
    title: 'Login Screen'
  }

  onEmailChange(text) {
    this.props.emailChanged(text);
  }

  onPasswordChange(text) {
    this.props.passwordChanged(text);
  }

  onButtonPress() {
    const { email, password } = this.props;
    this.props.loginUser({ email, password }, () => {
      this.props.navigation.navigate('details');
    });
  }

  renderButton() {
    if (this.props.loading) {
      return (
        <Spinner size="large" />
      );
    }
    return (
      <Button
      onPress={this.onButtonPress.bind(this)}
      title="Login"
      icon={{ name: 'done' }}
      backgroundColor='#03A9F4'
      borderRadius={75}
      />
    );
  }

  render() {
    return (
      <View>
      <Card>
        <View>
          <FormLabel>Email</FormLabel>
          <FormInput
            placeholder="email@gmail.com"
            onChangeText={this.onEmailChange.bind(this)}
            value={this.props.email}
          />
        </View>
        <View>
          <FormLabel>Password</FormLabel>
          <FormInput
            label="Password"
            placeholder="password"
            secureTextEntry
            onChangeText={this.onPasswordChange.bind(this)}
            value={this.props.password}
          />
        </View>

        <FormValidationMessage>{this.props.error}</FormValidationMessage>

        <View>
          {this.renderButton()}
        </View>
        <View style={{ marginTop: 10 }}>
          <Button
            onPress={() => this.props.navigation.navigate('register')}
            title="Dont have account?"
            icon={{ name: 'forward' }}
            backgroundColor='#009688'
            borderRadius={75}
          />
        </View>
      </Card>
      </View>
    );
  }
}

const mapStateToprops = ({ auth }) => {
  const { email, password, error, loading } = auth;

  return { email, password, error, loading };
};

const styles = {
  errorTextStyle: {
    fontSize: 20,
    alignSelf: 'center',
    color: 'red'
  }
};

export default connect(mapStateToprops, actions)(LoginForm);
