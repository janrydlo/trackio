import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Card, Button, Icon } from 'react-native-elements';
import * as actions from '../actions';

import Swipe from '../components/Swipe';
import { Spinner } from '../components/Spinner';

class DiscoverScreen extends Component {
  static navigationOptions = {
    title: 'Discover',
    tabBarIcon: ({ tintColor }) => {
      return <Icon name="visibility" size={30} color={tintColor} />;
    }
  }

  componentWillMount() {
    this.props.fetchShowsDiscover();
  }

  renderImage(image) {
    if (_.isNull(image)) {
      return <View />;
    }
    return (
      <View>
        <Image source={{ uri: image.original }} style={{ height: 150 }} />
      </View>
    );
  }

 renderCard = (item) => {
   const { name, image, rating, genres, summary } = item.show;
   return (
      <Card title={name}>
          {this.renderImage(image)}
       <View style={styles.detailWrapper}>
         <Text>
          {`${'Rating: '}${rating.average == null ? 'N/A' : rating.average}`}
         </Text>
         <Text>
          {`${'Genres: '}${_.isEmpty(genres) ? 'N/A' : genres.join(', ')}`}
         </Text>
       </View>
       <Text>
         {summary.replace(/<[^>]*>/g, '').substring(0, 300)}
       </Text>
     </Card>
   );
 }

 renderNoMoreCards = () => (
      <Card title="No More Shows to Discover">
        <Button
          title="Find more!"
          large
          icon={{ name: 'home' }}
          backgroundColor="#03A9F4"
          onPress={() => this.props.fetchShowsDiscover()}
        />
      </Card>
    );

  renderScreen() {
    if (this.props.loading) {
      return (
        <View style={{ flex: 1 }}>
          <Spinner size="large" />
        </View>
      );
    }
    return (
      <View style={{ flex: 1 }}>
        <Swipe
          data={this.props.discoveredShows}
          renderCard={this.renderCard}
          renderNoMoreCards={this.renderNoMoreCards}
          onSwipeRight={show => this.props.showSave(show)}
          keyProp="id"
        />
      </View>
    );
  }

  render() {
    return (
        this.renderScreen()
    );
  }
}


const styles = {
  detailWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginBottom: 10,
    marginTop: 10
  }
};

const mapStateToProps = ({ addedShows }) => {
  const sortedList = _.orderBy(
    addedShows.discoveredShows, ['show.rating.average'], ['desc']);
  const filteredList = _.filter(sortedList, item => !_.isNil(item.show.rating.average));
  const shuffledList = _.shuffle(filteredList);
  return {
      discoveredShows: shuffledList.slice(0, 10),
      loading: addedShows.loading
  };
};

export default connect(mapStateToProps, actions)(DiscoverScreen);
