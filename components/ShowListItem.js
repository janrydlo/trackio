import React, { Component } from 'react';
import { Text, TouchableWithoutFeedback, View, Image } from 'react-native';
import { Card } from 'react-native-elements';
import _ from 'lodash';

class ShowListItem extends Component {

  onRowPress() {

  }

  renderImage(image) {
    if (_.isNull(image)) {
      return <View />;
    }
    return (
      <View style={{ flex: 1, flexDirection: 'row' }}>
        <Image source={{ uri: image.original }} style={{ flex: 1, height: 250 }} />
      </View>
    );
  }

  render() {
    const show = this.props.show.show.show;
    return (
      <TouchableWithoutFeedback key={show.id} onPress={this.onRowPress.bind(this)}>
      <Card title={show.name}>
        {this.renderImage(show.image)}
        <View style={{ height: 200 }}>
          <View style={styles.detailWrapper}>
            <Text style={styles.italics}>{show.status}</Text>
            <Text style={styles.italics}>{show.premiered}</Text>
          </View>
          <View>
           <Text>
             {show.summary.replace(/<[^>]*>/g, '').substring(0, 100)}
           </Text>
          </View>
        </View>
      </Card>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = {
  titleStyle: {
    fontSize: 18,
    paddingLeft: 15
  },
  imageContainerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start'
  }
};

export default ShowListItem;
