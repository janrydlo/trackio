import React, { Component } from 'react';
import { AppLoading } from 'expo';
import _ from 'lodash';
import firebase from 'firebase';

import Slides from '../components/Slides';

const SLIDE_DATA = [
  { text: 'Welcome to TrackIO!', color: '#03A9F4' },
  { text: 'Use this to track progress in your favorite TV Shows!', color: '#009688' },
  { text: 'Just select your TV Show and swipe episodes away!', color: '#03A9F4' }
];

class WelcomeScreen extends Component {
  state = { currentUser: null }

  componentWillMount() {
    const { currentUser } = firebase.auth();
    console.log(currentUser);
    if (currentUser) {
      this.setState({ currentUser });
      this.props.navigation.navigate('review');
    } else {
      this.setState({ currentUser: false });
    }
  }

  onSlidesComplete = () => {
    this.props.navigation.navigate('auth');
  }

  render() {
    if (_.isNull(this.state.currentUser)) {
      return <AppLoading />;
    }
    return (
      <Slides data={SLIDE_DATA} onComplete={this.onSlidesComplete} />
    );
  }
}

export default WelcomeScreen;
