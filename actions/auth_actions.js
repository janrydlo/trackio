import { AsyncStorage } from 'react-native';
import { Facebook } from 'expo';
import firebase from 'firebase';
import _ from 'lodash';

import {
  FACEBOOK_LOGIN_SUCCESS,
  FACEBOOK_LOGIN_FAIL,
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  REPEATED_PASSWORD_CHANGED,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_USER,
  REGISTER_USER,
  REGISTER_USER_SUCCESS,
  REGISTER_USER_FAIL
} from './types';

export const facebookLogin = () => async dispatch => {
  let token = await AsyncStorage.getItem('fb_token');

  if (token) {
    dispatch({ type: FACEBOOK_LOGIN_SUCCESS, payload: token });
  } else {
    doFacebookLogin(dispatch);
  }
};

const doFacebookLogin = async dispatch => {
  let { type, token } = await Facebook.logInWithReadPermissionsAsync('391006957961886', {
    permissions: ['public_profile']
  });

  if (type === 'cancel') {
    return dispatch({ type: FACEBOOK_LOGIN_FAIL });
  }

  await AsyncStorage.setItem('fb_token', token);
  dispatch({ type: FACEBOOK_LOGIN_SUCCESS, payload: token });
};


export const emailChanged = (text) => {
  return {
    type: EMAIL_CHANGED,
    payload: text
  };
};

export const passwordChanged = (text) => {
  return {
    type: PASSWORD_CHANGED,
    payload: text
  };
};

export const repeatedPasswordChanged = (text) => {
  return {
    type: REPEATED_PASSWORD_CHANGED,
    payload: text
  };
};

export const loginUser = ({ email, password }, callback) => {
  return (dispatch) => {
    dispatch({ type: LOGIN_USER });

    firebase.auth().signInWithEmailAndPassword(email, password)
      .then(user => {
        loginUserSucces(dispatch, user);
        callback();
      })
      .catch(() => {
        firebase.auth().createUserWithEmailAndPassword(email, password)
          .then(user => loginUserSucces(dispatch, user))
          .catch(() => loginUserFail(dispatch));
      });
  };
};

const loginUserSucces = (dispatch, user) => {
  dispatch({
    type: LOGIN_USER_SUCCESS,
    payload: user
  });
};

const loginUserFail = (dispatch) => {
  dispatch({
    type: LOGIN_USER_FAIL
  });
};

export const registerUser = ({ email, password, repeatedPassword }, callback) => {
  return (dispatch) => {
    dispatch({ type: REGISTER_USER });

    if (!_.isEqual(password, repeatedPassword)) {
      registerUserFail(dispatch, 'Your passwords are different.');
    } else {
      firebase.auth().createUserWithEmailAndPassword(email, password)
        .then(user => registerUserSuccess(dispatch, user, callback))
        .catch((error) => registerUserFail(dispatch, error.message));
    }
  };
};

const registerUserSuccess = (dispatch, user, callback) => {
  dispatch({
    type: REGISTER_USER_SUCCESS,
    payload: user
  });
  callback();
};

const registerUserFail = (dispatch, error) => {
  dispatch({
    type: REGISTER_USER_FAIL,
    payload: error
  });
};
