import Expo from 'expo';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { TabNavigator, StackNavigator } from 'react-navigation';
import { Provider } from 'react-redux';
import firebase from 'firebase';

import store from './store';
import WelcomeScreen from './screens/WelcomeScreen';
import LoginScreen from './screens/LoginScreen';
import ReviewScreen from './screens/ReviewScreen';
import DiscoverScreen from './screens/DiscoverScreen';
import EpisodesReviewScreen from './screens/EpisodesReviewScreen';
import SettingsScreen from './screens/SettingsScreen';
import ShowsListScreen from './screens/ShowsListScreen';
import RegistrationScreen from './screens/RegistrationScreen';
import ShowDetailScreen from './screens/ShowDetailScreen';


class App extends React.Component {
  componentWillMount() {
    const config = {
      apiKey: 'AIzaSyA1F4bICqqEL45sM-Bh4QRO5-bmuIS-yVQ',
      authDomain: 'trackio-fa0ca.firebaseapp.com',
      databaseURL: 'https://trackio-fa0ca.firebaseio.com',
      projectId: 'trackio-fa0ca',
      storageBucket: 'trackio-fa0ca.appspot.com',
      messagingSenderId: '40657874974'
    };

   firebase.initializeApp(config);
 }

  render() {
    const MainNavigator = TabNavigator({
      welcome: { screen: WelcomeScreen },
      auth: { screen: StackNavigator({
        auth: { screen: LoginScreen },
        register: { screen: RegistrationScreen }
        })
      },
      main: {
        screen: TabNavigator({
          details: {
            screen: StackNavigator({
              review: { screen: ReviewScreen },
              showDetail: { screen: ShowDetailScreen },
              episodesReview: { screen: EpisodesReviewScreen },
              showsList: { screen: ShowsListScreen }
            }),
           },
          discover: { screen: StackNavigator({
            discover: { screen: DiscoverScreen }
          })
        },
        }, {
          tabBarPosition: 'bottom',
          tabBarOptions: {
            labelStyle: { fontSize: 12 }
          },
          lazy: true
        })
      }
    }, {
      navigationOptions: {
        tabBarVisible: false
      },
      lazy: true
    });

    return (
      <Provider store={store}>
        <View style={styles.container}>
            <MainNavigator />
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
});

Expo.registerRootComponent(App);
