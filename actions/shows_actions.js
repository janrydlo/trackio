import axios from 'axios';
import firebase from 'firebase';

import {
  FETCH_SINGLE_SHOW,
  FETCH_SHOWS,
  FETCH_DISCOVER,
  USER_SHOWS_FETCH,
  USER_SHOWS_FETCH_SUCCESS,
  SHOW_SAVE_SUCCESS,
  SHOW_UPDATE_SUCCESS,
  TEXT_CHANGED,
  NOTE_UPDATE_SUCCESS,
  FETCH_DISCOVER_SUCCESS,
  SHOW_DELETE_SUCCESS
} from './types';

const BASE_API_URL = 'http://api.tvmaze.com';

export const fetchSingleShowByName = name => async (dispatch) => {
  try {
    let { data } = await axios.get(`${BASE_API_URL}${'/singlesearch/shows?q='}${name}`);
    dispatch({ type: FETCH_SINGLE_SHOW, payload: data });
  } catch (e) {
    console.log(e);
  }
};

export const fetchShowsByName = (name, callback) => async (dispatch) => {
  try {
    let { data } = await axios.get(`${BASE_API_URL}${'/search/shows?q='}${name}`);
    dispatch({ type: FETCH_SHOWS, payload: data });
    callback();
  } catch (e) {
    console.log(e.message);
  }
};

export const fetchShowsDiscover = () => async (dispatch) => {
  try {
    dispatch({ type: FETCH_DISCOVER });
    let { data } = await axios.get(`${BASE_API_URL}${'/schedule?country=US&date=2017-06-01'}`);
    dispatch({ type: FETCH_DISCOVER_SUCCESS, payload: data });
  } catch (e) {
    console.log(e.message);
  }
};

export const showsFetch = () => {
    const { currentUser } = firebase.auth();

    return (dispatch) => {
      dispatch({ type: USER_SHOWS_FETCH });
      firebase.database().ref(`/users/${currentUser.uid}/shows`)
        .on('value', snapshot => {
          dispatch({ type: USER_SHOWS_FETCH_SUCCESS, payload: snapshot.val() });
        });
    };
};

export const showSave = (show, callback) => {
  const { currentUser } = firebase.auth();

  return (dispatch) => {
    firebase.database().ref(`/users/${currentUser.uid}/shows/${show.show.id}`)
      .set({
        show
      })
      .then(() => {
        dispatch({
          type: SHOW_SAVE_SUCCESS
        });
        if (callback) {
          callback();
        }
       });
  };
};

export const saveLastEpisode = (showId, episode, callback) => {
  const { currentUser } = firebase.auth();

  return (dispatch) => {
    firebase.database().ref(`/users/${currentUser.uid}/shows/${showId}`)
      .update({
        lastSeen: episode
      })
      .then(() => {
        dispatch({
          type: SHOW_UPDATE_SUCCESS
        });
        callback();
    });
  };
};

export const saveShowRating = (showId, value) => {
  const { currentUser } = firebase.auth();

  return (dispatch) => {
    firebase.database().ref(`/users/${currentUser.uid}/shows/${showId}`)
      .update({
        rating: value
      })
      .then(() => {
        dispatch({
          type: SHOW_UPDATE_SUCCESS
        });
    });
  };
};

export const saveShowNote = (showId, value) => {
  const { currentUser } = firebase.auth();

  return (dispatch) => {
    firebase.database().ref(`/users/${currentUser.uid}/shows/${showId}`)
      .update({
        note: value
      })
      .then(() => {
        dispatch({
          type: NOTE_UPDATE_SUCCESS
        });
    });
  };
};

export const deleteUserShow = showId => async dispatch => {
  const { currentUser } = firebase.auth();
  try {
    await firebase.database().ref(`/users/${currentUser.uid}/shows/${showId}`).remove();
    dispatch({ type: SHOW_DELETE_SUCCESS });
  } catch (e) {
    console.log(e.message);
  }
};

export const textChanged = (text) => {
  return {
    type: TEXT_CHANGED,
    payload: text
  };
};
