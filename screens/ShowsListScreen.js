import React, { Component } from 'react';
import { View, Text, ScrollView, Image } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Card, Button } from 'react-native-elements';
import * as actions from '../actions';

class ShowsListScreen extends Component {
  static navigationOptions = {
    title: 'Found Shows'
  }

  onButtonPress(show) {
   this.props.showSave(show, () => {
     this.props.navigation.navigate('review');
   });
 }

 renderImage(image) {
   if (_.isNull(image)) {
     return <View />;
   }
   return (
     <View>
       <Image source={{ uri: image.original }} style={{ height: 200 }} />
     </View>
   );
 }

  renderShows() {
    return this.props.shows.map(show => {
      const { id, name, image, status, network, summary } = show.show;
      return (
        <Card key={id} title={name}>
          {this.renderImage(image)}
          <View>
            <View style={styles.detailWrapper}>
              <Text style={styles.italics}>{_.isNil(network) ? 'Not available' : network.name }</Text>
              <Text style={styles.italics}>{status}</Text>
            </View>
            <View>
             <Text>
               {_.isNil(summary) ? '' : summary.replace(/<[^>]*>/g, '')}
             </Text>
            </View>
            <View style={{ marginTop: 10 }}>
             <Button
               backgroundColor="#03A9F4"
               title="Add to my collection"
               icon={{ name: 'done' }}
               onPress={() => this.onButtonPress(show)}
             />
            </View>
          </View>
        </Card>
      );
    });
  }

  render() {
    return (
      <View>
        <ScrollView>
          {this.renderShows()}
        </ScrollView>
      </View>
    );
  }
}

const styles = {
  detailWrapper: {
    marginBottom: 10,
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  italics: {
    fontStyle: 'italic'
  }
};

const mapStateToProps = ({ addedShows }) => {
  return (
    {
      show: addedShows.singleShow,
      shows: addedShows.shows
     }
  );
};

export default connect(mapStateToProps, actions)(ShowsListScreen);
